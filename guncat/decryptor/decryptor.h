#ifndef INCLUDED_DECRYPTOR_
#define INCLUDED_DECRYPTOR_

#include <string>
#include <fstream>
#include <memory>

class Options;
class GPGHandler;

class Decryptor
{
    Options const &d_options;
    GPGHandler &d_gpgHandler;

    std::unique_ptr<std::ofstream> d_fverbose;

    public:
        Decryptor(GPGHandler &gpgHandler);
                                                // -----BEGIN PGP MESSAGE-----
                                                // was just read by
        void handlePGP(std::istream &in,         // Guncat::process
                       std::ostream &out);

    private:
        void setVerbose();

        static void error(std::ostream &out, int ret, 
                          std::pair<unsigned, unsigned> const &range);
};

#endif





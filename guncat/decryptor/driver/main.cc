//#define XERR
#include "main.ih"

namespace
{
    string terminating{ "Terminating" };
}

int main(int argc, char **argv)
try
{
    if (argc == 1)
    {
        cerr << "file containing PGP SECTION required. If a second arg\n"
                "is provided the passphrase entered at -p is shown\n";
        return 1;
    }

    Arg const &arg = prepareArgs(argc, argv);
    (terminating += ' ') += arg.basename();

    Options const &options = Options::instance();

    ifstream in(arg[0]);

    Decryptor decryptor;
  
    if (options.getPassphrase())
    {
        string pass;
           
        OneKey key;
        cout << "? ";
        while (true)
        {
            int ch = key.get();
            if (ch == '\n')
                break;
            pass += ch;
        }

        if (arg.nArgs() > 1)
            cout << '`' << pass << "'\n";

        decryptor.setPassphrase(pass);
    }

    in.ignore(100, '\n');               // skip BEGIN PGP MESSAGE
    decryptor.handlePGP(in);
}
catch (int x)
{
    cerr << "returning " << x << '\n';
    return 0;
}
catch (exception const &exc)
{
    cerr << terminating << ": " << exc.what() << '\n';
    return 1;
}
catch (...)
{
    cerr << "Unexpected exception\n";
    return 1;
}

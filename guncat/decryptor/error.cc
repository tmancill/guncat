#include "decryptor.ih"

// static
void Decryptor::error(ostream &out, int ret, 
                      pair<unsigned, unsigned> const &range)
{
    ostringstream msg;

    msg << "In " << g_filename << ':' << range.first << ':' << range.second << 
            ": cannot decrypt PGP section, gpg returns " << ret;

    string msgTxt{ msg.str() };

    starred(out, msgTxt);

    if (g_verbose)
        *g_verbose << msgTxt << '\n';

    if (Options::instance().noErrors())
        throw Exception{} << msgTxt;
}

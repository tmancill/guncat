//#define XERR
#include "decryptor.ih"

void Decryptor::handlePGP(istream &in, ostream &out)
{
    PGPSection pgpSection{ in };                    // obtain the PGP MESSAGE

    if (g_verbose)
         *g_verbose << fileInfo() << ": PGP HEADER" << endl;

                                                    // got a complete
    if (pgpSection.verify(out))                     // PGP section
    {
                                                    // decrypt it unless only
        if (not d_options.pgpRanges())              // ranges are requested
        {
            if (int ret = d_gpgHandler.process(pgpSection.str()); ret != 0)
                error(out, ret, pgpSection.range());
        }

        return;
    }
                                                    // unless suppressed:
    if (not d_options.skipIncomplete())             // show the INcomplete
        out << pgpSection.str().rdbuf();            // PGP MESSAGE
}




#include "decryptor.ih"

void Decryptor::setVerbose()
{
    string const &verbose = d_options.verbose();

    if (not verbose.empty())                    // if a msgname was specified:
    {
        if (verbose == "-")
            g_verbose = &cerr;                      // write to cerr
        else
        {
            d_fverbose = unique_ptr<ofstream>{      // or open a file
                        new ofstream{ 
                            Exception::factory<ofstream>(verbose) 
                        }
                     };

            g_verbose = d_fverbose.get();
        }
    }
}

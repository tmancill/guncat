#ifndef INCLUDED_GLOBALS_H_
#define INCLUDED_GLOBALS_H_

#include <iosfwd>

extern size_t g_lineNr;

extern std::string g_filename;
extern std::string g_beginPGP;
extern std::string g_endPGP;

extern std::ostream *g_verbose;

std::string fileInfo();
std::istream &nextline(std::istream &in, std::string &line);
void  starred(std::ostream &out, std::string const &msg);

#endif

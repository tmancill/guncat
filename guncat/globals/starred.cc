#include "globals.ih"

void  starred(std::ostream &out, std::string const &msg)
{
    out << '\n' <<
            setfill('*') << setw(msg.length()) << '*' << '\n' <<
            msg << '\n' <<
            setw(msg.length()) << '*' << '\n' << setfill(' ') <<
            '\n';
}

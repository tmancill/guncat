//#define XERR
#include "gpghandler.ih"

void GPGHandler::checkPassphrase(istream &in)
{
    if (d_options.pgpRanges())          // no passphrase needed when merely
        return;                         // reporting ranges

    if (d_passphraseFirstLine)
        getline(in, d_passphrase);

    while (true)
    {
        fork();

        switch (d_ret)
        {
            case 2:
                retryPassphrase();
                resetPipes();
            break;

            default:            // some error...
            throw Exception{} << "gpg returned " << d_ret;

            case 0:                 // no errors
                d_gpgCommand = d_options.gpgPath() + d_options.gpgDecrypt();
            return;           // this section is now completed
      }
    }
}




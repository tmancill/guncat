#define XERR
#include "gpghandler.ih"

void GPGHandler::childProcess()
{
    Pipe pipe;

    Process gpg{ 
                Process::DIRECT, Process::NO_PATH, 
                d_gpgCommand  + passphraseFd(pipe) 
            };

    gpg.setTimeLimit(d_options.timeLimit());            // 0 -> no time limit

    gpg.start();
}




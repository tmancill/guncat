//#define XERR
#include "gpghandler.ih"

void GPGHandler::childRedirections()
{
    d_childInput.readFrom(Redirector::STDIN);
    d_childOutput.writtenBy(Redirector::STDOUT);
    d_childMessages.writtenBy(Redirector::STDERR);
}

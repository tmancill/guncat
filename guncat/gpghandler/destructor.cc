//#define XERR
#include "gpghandler.ih"

GPGHandler::~GPGHandler()
{
    d_childInput.close();
    d_childOutput.close();
}

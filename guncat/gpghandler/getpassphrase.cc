//#define XERR
#include "gpghandler.ih"

bool GPGHandler::getPassphrase()
{
    if (not d_passphrase.empty())   // a passphrase is already available
        return true;

    cerr << "Enter passphrase (empty line to skip): ";

    struct termios ttySaved;

    int fd = echo(&ttySaved);       // switch off echoing, fd to /dev/tty

    IFdStream in(fd);               // get the passphrase from /dev/tty
    getline(in, d_passphrase);

    echo(&ttySaved, true);          // restore the echoing state

    cerr << '\n';

    return not d_passphrase.empty();
}


#ifndef INCLUDED_GPGHANDLER_
#define INCLUDED_GPGHANDLER_

#include <iosfwd>

#include <bobcat/fork>
#include <bobcat/pipe>

class Options;

class GPGHandler: public FBB::Fork
{
    std::ostream &d_out;

    Options const &d_options;
    std::istream *d_in;

    bool d_checkPassphrase;
    bool d_passphraseFirstLine;

    std::string d_gpgCommand;
    std::string d_passphrase;

    FBB::Pipe d_childInput;
    FBB::Pipe d_childOutput;
    FBB::Pipe d_childMessages;

    int d_ret;                              // return value of the gpg call

    static unsigned s_nRetries;             // # times a pwd was requested

    public:
                                            // passphrase may have been 
                                            // provided via the 1st input line
        GPGHandler(std::ostream &out, bool passphraseFirstLine);  
        ~GPGHandler() override;             // closes the pipes

        void checkPassphrase(std::istream &in);

        int process(std::istream &in);

    private:
        std::string passphraseFd(FBB::Pipe &pipe) const;

        bool getPassphrase();           // get the passphrase from /dev/tty
        void retryPassphrase();
        void resetPipes();

        void childRedirections() override;
        void childProcess()      override;
        void parentProcess()     override;

        static int echo(struct termios *ttySaved, bool reset = false);
};
     
//    
#endif

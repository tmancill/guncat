//#define XERR
#include "gpghandler.ih"

GPGHandler::GPGHandler(ostream &out, bool passphraseFirstLine)
:
    d_out(out),
    d_options(Options::instance()),
    d_in(0),
    d_checkPassphrase(true),
    d_passphraseFirstLine(passphraseFirstLine),
    d_gpgCommand(d_options.gpgPath() + d_options.gpgSign())
{
    xerr("Initial GPG command: " << d_gpgCommand);
}


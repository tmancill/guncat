#define XERR
#include "gpghandler.ih"

void GPGHandler::parentProcess()
{
    IFdStream fromChild(d_childOutput.readOnly(), 1000);
    IFdStream childMessages(d_childMessages.readOnly(), 500);

    {
        OFdStream toChild(d_childInput.writeOnly(), 1000);
        if (d_checkPassphrase)
            toChild << "sign\n";
        else
            toChild << d_in->rdbuf();
    }

    d_ret = waitForChild();

    if (d_ret != 0)
        return;
 
    if (d_checkPassphrase)
        d_checkPassphrase = false;
    else if (d_options.quotedPrintable())       // accept quoted print. output
        d_out << fromChild.rdbuf();
    else
    {
        IQuotedPrintableBuf<DECODE> decode{ fromChild };
        istream din(&decode);
 
        d_out << din.rdbuf();
    }
}


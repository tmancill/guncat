#define XERR
#include "gpghandler.ih"

int GPGHandler::process(istream &in)
{
    d_in = &in;
    
    resetPipes();
    fork();

    return d_ret;           // this section is now completed
}



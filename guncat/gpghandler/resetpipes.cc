//#define XERR
#include "gpghandler.ih"

void GPGHandler::resetPipes()
{
    d_childInput.close();
    d_childInput = Pipe{};

    d_childOutput.close();
    d_childOutput = Pipe{};

    d_childMessages.close();
    d_childMessages = Pipe{};
}

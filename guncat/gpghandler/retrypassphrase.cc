#include "gpghandler.ih"

void GPGHandler::retryPassphrase()
{
    if (d_passphraseFirstLine)
        throw Exception{} << "Incorrect passphrase on 1st line of input";

    switch (++s_nRetries)
    {
        case 1:
        break;

        case 4:
        throw Exception{} << "Quitting after three passphrase attempts\n";

        default:
            cerr << "Incorrect passphrase. Try again...\n";
        break;
    }

    d_passphrase.clear();
    getPassphrase();
}





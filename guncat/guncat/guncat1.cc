#define XERR

#include "guncat.ih"

Guncat::Guncat()
:
    d_options(Options::instance()),

    d_ofstream(outputStream(d_options)),
    d_out(d_options.stdOut() ? cout : *d_ofstream.get()),

    d_gpgCommand(d_options.gpgCommand()),
    d_pgpRanges(d_options.pgpRanges()),
    d_reduceHeaders(d_options.reduceHeaders()),
    d_gpgHandler(d_out, d_options.passphraseFirstLine()),
    d_decryptor(d_gpgHandler)
{}

#include "guncat.ih"

bool Guncat::onlyCin()
{
    Arg &arg = Arg::instance();

    switch (arg.nArgs())
    {
        case 1:                     // 1 argument:
            if (arg[0] != "-"s)     // if not - then its a filename
                break;
        [[fallthrough]];

        case 0:                     // no or 1 argument: read stdin
            g_filename = "stdin";
            d_gpgHandler.checkPassphrase(cin);
            process(cin);
        return true;

        default:                    // multiple args: may not contain -
        {
            auto begin = arg.argPointers();
            auto end = begin + arg.nArgs();
            auto iter = find(begin, end, "-");
            delete[] begin;

            if (iter != end)
                throw Exception{} << 
                    "argument - cannot be mixed with filename arguments";
        }
        break;
    }

    return false;
}




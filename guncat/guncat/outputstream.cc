#include "guncat.ih"

// static
ostream *Guncat::outputStream(Options const &options)
{
    string path{ options.pipePath() };

    if (not path.empty())
        return new OOutputStream{ path };

    path = options.writePath();

    return 
        path.empty() ?  
            0 : 
            new ofstream{ Exception::factory<ofstream>(path) };
}

#include "guncat.ih"

void Guncat::programArguments()
{
    if (onlyCin())
        return;

    Arg &arg = Arg::instance();

    for (size_t idx = 0, end = arg.nArgs(); idx != end; ++idx)
    {
        g_filename = arg[idx];
        ifstream in{ Exception::factory<ifstream>(g_filename) };

        if (idx == 0)                           // set passphrase before
            d_gpgHandler.checkPassphrase(in);   // processing the 1st files

        process(in);
    }
}

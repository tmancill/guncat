#define XERR
#include "guncat.ih"

bool Guncat::reduceHeaders(string const &line)
{
    if (line.find_first_not_of(" \t") == string::npos)  // no non-blank chars
        return false;

    if (
        find_if(s_accept, s_acceptEnd,
            [&](char const *target)
            {
                return line.find(target) == 0;
            }
        ) 
        != s_acceptEnd
    )
        d_out << line << '\n';

    return true;
}

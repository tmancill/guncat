#include "main.ih"

namespace
{
    string terminating{ "Terminating" };
}

int main(int argc, char **argv)
try
{
    Arg const &arg = prepareArgs(argc, argv);

    (terminating += ' ') += arg.basename();

    arg.versionHelp(usage, version, 0);

                                            // no stdin redirection, no files
    if (                                    // and not --gpg-command?
        not Options::instance().gpgCommand() and
            isatty(STDIN_FILENO) and arg.nArgs() == 0
    )
    {
        usage(arg.basename());
        throw 1;
    }
    
    Guncat guncat;
    guncat.run();                           // process all files
}
catch (int x)
{
    return x;
}
catch (exception const &exc)
{
    cerr << terminating << ": " << exc.what() << '\n';
    return 1;
}
catch (...)
{
    cerr << "Unexpected exception\n";
    return 1;
}

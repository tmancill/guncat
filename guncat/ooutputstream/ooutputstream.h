#ifndef INCLUDED_OOUTPUTSTREAM_
#define INCLUDED_OOUTPUTSTREAM_

#include <iosfwd>
#include <ostream>

#include "../outputbuf/outputbuf.h"

class OOutputStream:  private OutputBuf, public std::ostream
{
    public:
        OOutputStream(std::string const &programName);
        ~OOutputStream();

    private:
};
        
#endif

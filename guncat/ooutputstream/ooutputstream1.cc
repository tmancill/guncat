//#define XERR
#include "ooutputstream.ih"

OOutputStream::OOutputStream(string const &programPath)
:
    OutputBuf(programPath),
    std::ostream(this)
{}

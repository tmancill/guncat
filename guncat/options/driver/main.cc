#include "main.ih"

namespace
{
    string terminating{ "Terminating" };
}

int main(int argc, char **argv)
try
{
    Arg const &arg = prepareArgs(argc, argv);
    (terminating += ' ') += arg.basename();

    Options const &options = Options::instance();

    cout <<
        "getPassphrase: " << options.getPassphrase() << "\n"
        "pgpRanges: " << options.pgpRanges()    << "\n"
        "noErrors: " << options.noErrors()      << "\n"
        "gpgPath: " << options.gpgPath()  << "\n"

        "gpgOptions: " << options.gpgOptions()  << "\n"
        "gpgProgram: " << options.gpgProgram()  << "\n"
        "msgName: " << options.msgName()        << "\n"

        "verbose: " << options.verbose() << '\n';
}
catch (int x)
{
    cerr << "returning " << x << '\n';
    return 0;
}
catch (exception const &exc)
{
    cerr << "Terminating" << Arg::instance().basename() << ": " <<
            exc.what() << '\n';
    return 1;
}
catch (...)
{
    cerr << "Unexpected exception\n";
    return 1;
}

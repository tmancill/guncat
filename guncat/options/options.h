#ifndef INCLUDED_OPTIONS_
#define INCLUDED_OPTIONS_

#include <string>

namespace FBB
{
    class Arg;
}

class Options
{
    FBB::Arg const &d_arg;

    bool d_noErrors;
    bool d_passphraseFirstLine;               // from 1st input line
    bool d_gpgCommand;
    bool d_pgpRanges;
    bool d_sectionLines;
    bool d_reduceHeaders;
    bool d_skipIncomplete;
    bool d_quotedPrintable;

    std::string d_gpgDecrypt;               // options for decrypting
    std::string d_gpgSign;                  // options for signing
    std::string d_gpgPath; 
    std::string d_pipePath; 
    std::string d_writePath; 

    std::string d_verbose;

    size_t d_timeLimit;

    static Options *s_options;

    public:
        static Options const &instance();

        bool passphraseFirstLine() const;
        bool gpgCommand() const;
        bool pgpRanges() const;             // only report PGP section ranges
        bool reduceHeaders() const;
        bool sectionLines() const;

                                            // skip incomplete PGP sections
        bool skipIncomplete() const;        // (implied by pgpRanges)

        bool stdOut() const;

        bool quotedPrintable() const;

        bool noErrors() const;

        std::string const &gpgDecrypt() const;
        std::string const &gpgSign() const;
        std::string const &gpgPath() const;

        std::string const &pipePath() const;
        std::string const &writePath() const;

        std::string const &verbose() const;

        size_t timeLimit() const;

    private:
        Options();

        void extraGPGoptions();             // add --gpg-option values
};
        
inline bool Options::passphraseFirstLine() const
{
    return d_passphraseFirstLine;
}
        
inline bool Options::gpgCommand() const
{
    return d_gpgCommand;
}
        
inline bool Options::pgpRanges() const
{
    return d_pgpRanges;
}

inline bool Options::reduceHeaders() const
{
    return d_reduceHeaders;
}

inline bool Options::sectionLines() const
{
    return d_sectionLines;
}

inline bool Options::quotedPrintable() const
{
    return d_quotedPrintable;
}
        
inline bool Options::skipIncomplete() const
{
    return d_skipIncomplete;
}
        
inline bool Options::noErrors() const
{
    return d_noErrors;
}

inline bool Options::stdOut() const
{
    return d_pipePath.empty() and d_writePath.empty();
}

inline std::string const &Options::pipePath() const
{
    return d_pipePath;
}

inline std::string const &Options::writePath() const
{
    return d_writePath;
}
        
inline std::string const &Options::gpgSign() const
{
    return d_gpgSign;
}

inline std::string const &Options::gpgDecrypt() const
{
    return d_gpgDecrypt;
}

inline std::string const &Options::gpgPath() const
{
    return d_gpgPath;
}

inline std::string const &Options::verbose() const
{
    return d_verbose;
}

inline size_t Options::timeLimit() const
{
    return d_timeLimit;
}

#endif






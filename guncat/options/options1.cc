#define XERR
#include "options.ih"

Options::Options()
:
    d_arg(Arg::instance()),
    d_noErrors(d_arg.option(0, "no-errors")),
    d_passphraseFirstLine(d_arg.option('p')),
    d_gpgCommand(d_arg.option(0, "gpg-command")),
    d_pgpRanges(d_arg.option('r')),
    d_sectionLines(d_arg.option('S')),
    d_reduceHeaders(d_arg.option('R')),
    d_skipIncomplete(d_pgpRanges or d_arg.option('s')),
    d_quotedPrintable(d_arg.option('q')),

    d_gpgDecrypt(" --no-auto-key-locate --decrypt --batch"),
    d_gpgSign(" --no-auto-key-locate --quiet --no-tty --batch --sign")
{
                                            // override default gpg location
    if (not d_arg.option(&d_gpgPath, "gpg-path"))    
        d_gpgPath = "/usr/bin/gpg";

                                            // get the name of the verbosity
    d_arg.option(&d_verbose, 'V');          // file

    if (not d_arg.option('t'))              // by default --no-tty is used
        d_gpgDecrypt += " --no-tty";

    if (string timeLimit; d_arg.option(&timeLimit, 'T'))
        d_timeLimit = stoul(timeLimit);    

    extraGPGoptions();                      // add extra gpg options

    bool less = d_arg.option('l');          // --less was specified
    d_arg.option(&d_writePath, 'W');
    if (d_arg.option(&d_pipePath, 'P') and (not d_writePath.empty()  or less))
        throw Exception{} << 
                "options --less, --pipe and --write are mutually exclusive";

    if (less)
        d_pipePath = "/usr/bin/less";
    else if (
        error_code ec;
        not d_writePath.empty() and fs::exists(d_writePath, ec)
    )
        throw Exception{} << "--write " << d_writePath << 
                                                    ": file already exists";
            
}

//#define XERR
#include "outputbuf.ih"

void OutputBuf::childProcess()
{
    Process binary{ Process::DIRECT, Process::NO_PATH, d_programPath };

    binary.start();
}

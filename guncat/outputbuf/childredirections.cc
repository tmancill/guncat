//#define XERR
#include "outputbuf.ih"

void OutputBuf::childRedirections()
{
    xerr("child Redirections 1: " << d_childInput.readFd());

    d_childInput.readFrom(Redirector::STDIN);

    xerr("child Redirections 2: " << d_childInput.readFd());
};

//#define XERR
#include "outputbuf.ih"

OutputBuf::~OutputBuf()
{
    d_toChild->flush();

    d_childInput.close();
    waitForChild();

    delete d_toChild;
}

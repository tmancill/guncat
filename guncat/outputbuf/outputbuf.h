#ifndef INCLUDED_OUTPUTBUF_
#define INCLUDED_OUTPUTBUF_

#include <streambuf>
#include <string>

#include <bobcat/fork>
#include <bobcat/pipe>

#include <bobcat/ofdstream>

class OutputBuf: public std::streambuf, public FBB::Fork
{
    std::string d_programPath;
    FBB::Pipe d_childInput;
    FBB::OFdStream *d_toChild;

    public:
        OutputBuf(std::string const &programPath);
        ~OutputBuf() override;

    private:
        int overflow(int ch) override;

        void childRedirections() override;
        void childProcess() override;
        void parentProcess() override;
};

//inline void OutputBuf::close()
//{
//    overflow(EOF);
//}
        
#endif

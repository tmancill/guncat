//#define XERR
#include "outputbuf.ih"

int OutputBuf::overflow(int ch)
{
    d_toChild->put(ch);
    return ch;
}

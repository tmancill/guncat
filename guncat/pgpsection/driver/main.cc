#include "main.ih"

namespace
{
    string terminating{ "Terminating" };
}

int main(int argc, char **argv)
try
{
    if (argc == 1)
    {
        cout << "provide the filename containing PGP MESSAGEs\n";
        return 1;
    }

    Arg const &arg = prepareArgs(argc, argv);
    (terminating += ' ') += arg.basename();

    ifstream in{ arg[0] };
    
    PGPSection section(in);

    string line;
    while (getline(in, line))
    {
        ++g_lineNr;

        if (line == g_beginPGP)
            section.verify();
    }
}
catch (int x)
{
    cerr << "returning " << x << '\n';
    return 0;
}
catch (exception const &exc)
{
    cerr << terminating << ": " << exc.what() << '\n';
    return 1;
}
catch (...)
{
    cerr << "Unexpected exception\n";
    return 1;
}

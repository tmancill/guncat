//#define XERR
#include "pgpsection.ih"

bool PGPSection::empty()
{
    while (true)
    {
        if (d_line.empty())
        {
            next();
            nextLine();                 // get the line beyond the empty line
            return true;
        }
                                        // empty() also accepts initial 
                                        // content containing : beyond col. 0
        if (size_t pos = d_line.find(':');  pos == 0 or pos == string::npos)
            break;

        nextLine();
    }

    verbose("no initial empty line");
    return false;
}


//#define XERR
#include "pgpsection.ih"

bool PGPSection::lines()
{
    size_t length = d_line.length();

    while (nextLine())
    {
        if (not validChars())
            return false;

                                            // stop at a line of different
        if (length != d_line.length())      // length
        {
            next();                         
            return true;
        }
    }

    return false;                           // there must be a shorter line
}

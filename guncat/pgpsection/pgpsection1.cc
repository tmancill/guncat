//#define XERR
#include "pgpsection.ih"

PGPSection::PGPSection(istream &in)
:
    d_in(in),
    d_entryOffset(in.tellg()),
    d_offset(d_entryOffset)
{
    Options const &options = Options::instance();
    d_pgpRanges = options.pgpRanges() | options.sectionLines();
}

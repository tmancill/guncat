//#define XERR
#include "pgpsection.ih"

bool PGPSection::verify(ostream &out)
{
    d_tempStream << g_beginPGP << '\n';

    d_firstLine = g_lineNr;

    next();                         // BEGIN PGP MESSAGE has already been read
    nextLine();

    if (
            empty()         
        and lines()  
        and lastLine() 
        and endMessage()
    )
    {
        if (d_pgpRanges)
        {
            ostringstream str;
            str << g_filename << ':' << d_firstLine << ':' << g_lineNr << 
                    ": PGP MESSAGE";
            starred(out, str.str());
        }

        verbose("complete");
        return true;
    }

    if (d_offset != 0)
    {
        if (not d_in.seekg(d_offset))
            throw Exception{} << "repositioning failed";

        g_lineNr = d_nextNr;
    }

    return false;
}





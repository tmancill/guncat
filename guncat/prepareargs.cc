#include <bobcat/arg>

using namespace FBB;

// initializing FBB::Arg is separated from main() so it can be used by
// the various driver programs: they declare 
//
//      Arg const &prepareArgs(int argc, char **argv);

namespace
{
    Arg::LongOption longOptions[] =
    {
        Arg::LongOption("gpg-command",      Arg::None),
        Arg::LongOption("gpg-path",         Arg::Required),
        Arg::LongOption("gpg-option",       Arg::Required),
        Arg::LongOption("help",             'h'),
        Arg::LongOption("less",             'l'),
        Arg::LongOption("no-errors",        Arg::None),
        Arg::LongOption("passphrase",       'p'),
        Arg::LongOption("pipe",             'P'),
        Arg::LongOption("pgp-ranges",       'r'),
        Arg::LongOption("quoted-printable", 'q'), 
        Arg::LongOption("reduce-headers",   'R'), 
        Arg::LongOption("section-lines",    'S'), 
        Arg::LongOption("skip-incomplete",  's'), 
        Arg::LongOption("time-limit",       'T'),
        Arg::LongOption("tty-OK",           't'),
        Arg::LongOption("verbose",          'V'),
        Arg::LongOption("version",          'v'),
        Arg::LongOption("write",            'W'),
    };

    auto longEnd = longOptions + std::size(longOptions);
}

Arg const &prepareArgs(int argc, char **argv)
{
    return Arg::initialize("hlpP:qRrsStT:vV:W:",
                           longOptions, longEnd, argc, argv);
}

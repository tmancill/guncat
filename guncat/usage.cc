//                     usage.cc

#include "main.ih"

namespace
{
    char const info[] = R"R([options] [file(s)]
   Where:
   [options] - optional arguments (short options between parentheses):

      --gpg-command: show the gpg command that would be used, and quit.
      --gpg-path path: path to the gpg program (default: /usr/bin/gpg).
      --gpg-option "spec": option `spec' is passed to gpg child processes
                  (multiple --gpg-option options may be specified).
      --help (-h): provide this help (ends guncat, returning 0).
      --less (-l): output is written to '/usr/bin/less'. By default output is
                  written to the std. output stream. This option cannot be
                  specified when either option --pipe or option --write is
                  specified.
      --no-errors: processing ends if an invalid PGP section is encountered or
                  if gpg returns a non-zero exit value. Otherwise processing
                  continues beyond the failing PGP section, reporting the line
                  numbers of the failing PGP section.
      --passphrase (-p): the passphrase is read (without being echoed) as the
                  first line from the first file processed by guncat (which
                  may be the redirected std input stream; otherwise the
                  passphrase is checked by gpg itself (using gpg-agent).
      --pipe (-P) Full path to a program receiving guncat's output. By default
                  output is written to the std. output stream. This option
                  cannot be specified when either option --less or option
                  --write is specified.
      --pgp-ranges (-r): the lines-ranges of complete PGP MESSAGE
                  sections are reported. No additional output is produced.
      --quoted-printable (-q): merely decrypt PGP messages, keeping their
                  quoted-printable content (by default quoted-printable
                  content like '=3D' is converted to ascii).
      --reduce-headers (-R): only output the mail headers Cc:, Date:, From:, 
                  Subject:, and To:.
      --section-lines (-S): in the output precede decrypted PGP sections by
                  their line numbers.
      --skip-incomplete (-s): incomple PGP MESSAGE sections are not outputted.
      --time-limit (-T) <seconds>: maximum allowed time in seconds for
                  decrypting an encrypted section (by default: no time limit
                  is used).
      --tty-OK (-t): by default gpg's option --no-tty is used.
      --verbose (-V) path: specify - to write additional messages to stderr,
                  otherwise messages are written to `path' (by default
                  messages are suppressed).
      --version (-v)   - show version information (ends guncat, returning 0).
      --write (-W) path: output is written to 'path'. If 'path' already exists
                  guncat terminates. By default output is written to the
                  std. output stream. This option cannot be specified when
                  either option --less or option --pipe is specified.

   [file(s)] - file(s) to process. Standard input redirection can also be
                  used. By default the processed information is written to the
                  standard output stream, but may also be written to file
                  (using --write or standard output redirection), or may be
                  sent to a the standard input stream of another program
                  (using --less to use /usr/bin/less or --pipe to specify
                  another program).

)R";

}

void usage(std::string const &progname)
{
    cout << "\n" <<
    progname << " by " << Icmbuild::author << "\n" <<
    progname << " V" << Icmbuild::version << " " << Icmbuild::years << "\n"
    "\n"
    "Usage: " << progname << info;
}

